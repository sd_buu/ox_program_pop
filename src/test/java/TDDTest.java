/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.waritphat.ox_program_pop.OX_program_POP;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author domem
 */
public class TDDTest {
    
    public TDDTest() {
    }
        @Test
    public void testCheckOWIN_Vertical1(){
        char table[][] = {{'O', '2', '3'},
                          {'O', '5', '6'},
                          {'O', '8', '9'}};
        char player = 'O';
        boolean end = true;
            assertEquals(true, OX_program_POP.checkOWin(table));
    }
            @Test
    public void testCheckOWIN_Vertical2(){
        char table[][] = {{'1', 'O', '3'},
                          {'4', 'O', '6'},
                          {'7', 'O', '9'}};
        char player = 'O';
        boolean end = true;
            assertEquals(true, OX_program_POP.checkOWin(table));
    }
            @Test
    public void testCheckOWIN_Vertical3(){
        char table[][] = {{'1', '2', 'O'},
                          {'4', '5', 'O'},
                          {'7', '8', 'O'}};
        char player = 'O';
        boolean end = true;
            assertEquals(true, OX_program_POP.checkOWin(table));
    }
        @Test
    public void testCheckOWIN_Horizontal1(){
        char table[][] = {{'O', 'O', 'O'},
                          {'4', '5', '6'},
                          {'7', '8', '9'}};
        char player = 'O';
        boolean end = true;
            assertEquals(true, OX_program_POP.checkOWin(table));
    }
            @Test
    public void testCheckOWIN_Horizontal2(){
        char table[][] = {{'1', '2', '3'},
                          {'O', 'O', 'O'},
                          {'7', '8', '9'}};
        char player = 'O';
        boolean end = true;
            assertEquals(true, OX_program_POP.checkOWin(table));
    }
            @Test
    public void testCheckOWIN_Horizontal3(){
        char table[][] = {{'1', '2', '3'},
                          {'4', '5', '6'},
                          {'O', 'O', 'O'}};
        char player = 'O';
        boolean end = true;
            assertEquals(true, OX_program_POP.checkOWin(table));
    }
    public void testCheckOWIN_diagonal1(){
        char table[][] = {{'O', '2', '3'},
                          {'4', 'O', '6'},
                          {'7', '8', 'O'}};
        char player = 'O';
        boolean end = true;
            assertEquals(true, OX_program_POP.checkOWin(table));
    }
            @Test
    public void testCheckOWIN_diagonal2(){
        char table[][] = {{'1', '2', 'O'},
                          {'4', 'O', '6'},
                          {'O', '8', '9'}};
        char player = 'O';
        boolean end = true;
            assertEquals(true, OX_program_POP.checkOWin(table));
    }
    @Test
    public void testCheckXWIN_Vertical1(){
        char table[][] = {{'X', '2', '3'},
                          {'X', '5', '6'},
                          {'X', '8', '9'}};
        char player = 'X';
        boolean end = true;
            assertEquals(true, OX_program_POP.checkXWin(table));
    }
            @Test
    public void testCheckXWIN_Vertical2(){
        char table[][] = {{'1', 'X', '3'},
                          {'4', 'X', '6'},
                          {'7', 'X', '9'}};
        char player = 'X';
        boolean end = true;
            assertEquals(true, OX_program_POP.checkXWin(table));
    }
            @Test
    public void testCheckXWIN_Vertical3(){
        char table[][] = {{'1', '2', 'X'},
                          {'4', '5', 'X'},
                          {'7', '8', 'X'}};
        char player = 'X';
        boolean end = true;
            assertEquals(true, OX_program_POP.checkXWin(table));
    }
        @Test
    public void testCheckXWIN_Horizontal1(){
        char table[][] = {{'X', 'X', 'X'},
                          {'4', '5', '6'},
                          {'7', '8', '9'}};
        char player = 'X';
        boolean end = true;
            assertEquals(true, OX_program_POP.checkXWin(table));
    }
            @Test
    public void testCheckXWIN_Horizontal2(){
        char table[][] = {{'1', '2', '3'},
                          {'X', 'X', 'X'},
                          {'7', '8', '9'}};
        char player = 'X';
        boolean end = true;
            assertEquals(true, OX_program_POP.checkXWin(table));
    }
            @Test
    public void testCheckXWIN_Horizontal3(){
        char table[][] = {{'1', '2', '3'},
                          {'4', '5', '6'},
                          {'X', 'X', 'X'}};
        char player = 'X';
        boolean end = true;
            assertEquals(true, OX_program_POP.checkXWin(table));
    }
    public void testCheckXWIN_diagonal1(){
        char table[][] = {{'X', '2', '3'},
                          {'4', 'X', '6'},
                          {'7', '8', 'X'}};
        char player = 'X';
        boolean end = true;
            assertEquals(true, OX_program_POP.checkXWin(table));
    }
            @Test
    public void testCheckXWIN_diagonal2(){
        char table[][] = {{'1', '2', 'X'},
                          {'4', 'X', '6'},
                          {'X', '8', '9'}};
        char player = 'X';
        boolean end = true;
            assertEquals(true, OX_program_POP.checkXWin(table));
    }        
}
