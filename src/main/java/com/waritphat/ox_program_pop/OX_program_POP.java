/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.ox_program_pop;

/**
 *
 * @author domem
 */
import java.util.Scanner;

public class OX_program_POP {

    static char[][] table = {{'1', '2', '3'},
    {'4', '5', '6'},
    {'7', '8', '9'}};
    static char player = 'O';
    static Scanner ip = new Scanner(System.in);
    static int num = 0;
    static boolean end = false;
    static int countturn = 0;

    public static void main(String[] args) {
        welcome();
        showTable();
        while (end != true) {
            turn();
            showTable();
            if (countturn == 10) {
                break;
            }
        }

        whoWin();
    }

    public static void welcome() {
        System.out.println("Welcome to OX-Game");
    }

    public static void showTable() {
        System.out.println(table[0][0] + " | " + table[0][1] + " | " + table[0][2]);
        System.out.println("--+---+--");
        System.out.println(table[1][0] + " | " + table[1][1] + " | " + table[1][2]);
        System.out.println("--+---+--");
        System.out.println(table[2][0] + " | " + table[2][1] + " | " + table[2][2]);
        countturn++;
    }

    public static void turn() {
        if (player == 'O') {
            System.out.println("Turn " + player + " : ");
            System.out.println("input number 1-9");
            getNum();
            changeNumtoOX(num);
            player = 'X';
        } else {
            System.out.println("Turn " + player + " : ");
            System.out.println("input number 1-9");
            getNum();
            changeNumtoOX(num);
            player = 'O';
        }
    }

    public static void getNum() {
        int number = ip.nextInt();
        num = number;
    }

    public static void changeNumtoOX(int num) {
        if (num == 1) {
            table[0][0] = player;
        } else if (num == 2) {
            table[0][1] = player;
        } else if (num == 3) {
            table[0][2] = player;
        } else if (num == 4) {
            table[1][0] = player;
        } else if (num == 5) {
            table[1][1] = player;
        } else if (num == 6) {
            table[1][2] = player;
        } else if (num == 7) {
            table[2][0] = player;
        } else if (num == 8) {
            table[2][1] = player;
        } else if (num == 9) {
            table[2][2] = player;
        }
        checkOWin(table);
        checkXWin(table);
    }

    public static boolean checkOWin(char table[][]) {
        if (table[0][0] == 'O' && table[0][1] == 'O' && table[0][2] == 'O'
                || table[1][0] == 'O' && table[1][1] == 'O' && table[1][2] == 'O'
                || table[2][0] == 'O' && table[2][1] == 'O' && table[2][2] == 'O'
                || table[0][0] == 'O' && table[1][0] == 'O' && table[2][0] == 'O'
                || table[0][1] == 'O' && table[1][1] == 'O' && table[2][1] == 'O'
                || table[0][2] == 'O' && table[1][2] == 'O' && table[2][2] == 'O'
                || table[0][0] == 'O' && table[1][1] == 'O' && table[2][2] == 'O'
                || table[0][2] == 'O' && table[1][1] == 'O' && table[2][0] == 'O') {
            player = 'O';
            end = true;
        }
        return end;
    }

    public static boolean checkXWin(char table[][]) {
        if (table[0][0] == 'X' && table[0][1] == 'X' && table[0][2] == 'X'
                || table[1][0] == 'X' && table[1][1] == 'X' && table[1][2] == 'X'
                || table[2][0] == 'X' && table[2][1] == 'X' && table[2][2] == 'X'
                || table[0][0] == 'X' && table[1][0] == 'X' && table[2][0] == 'X'
                || table[0][1] == 'X' && table[1][1] == 'X' && table[2][1] == 'X'
                || table[0][2] == 'X' && table[1][2] == 'X' && table[2][2] == 'X'
                || table[0][0] == 'X' && table[1][1] == 'X' && table[2][2] == 'X'
                || table[0][2] == 'X' && table[1][1] == 'X' && table[2][0] == 'X') {
            player = 'X';
            end = true;
        }
        return end;
    }

    public static void whoWin() {
        if (end == false) {
            System.out.println(">> Draw! <<");
        } else if (player == 'O') {
            System.out.println(">>X WIN! <<");
        } else if (player == 'X') {
            System.out.println(">>O WIN! <<");
        }
    }
}
        
